This repository provides a sketch of how [SAT Competition 2019](http://sat-race-2019.ciirc.cvut.cz/index.php) CNF formulae are laid out in our experiments.  Instead of placing GBs of data in the repository, we place the checksum of formulae (files) in the repository.  This serves two purposes:

 - It provides definitive information about the folder structure and the location of formulae within this structure as used in our experiments.
 - It enables parity check between the formulae used in our experiments and the original data set (or subsequent replication efforts).

## Attribution

Copyright (c) 2019, Venkatesh-Prasad Ranganath

Licensed under BSD 4-Clause “Original” or “Old” License (https://choosealicense.com/licenses/bsd-4-clause/)

**Authors:** Venkatesh-Prasad Ranganath
